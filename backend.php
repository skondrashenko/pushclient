<?php

const PUSH_SERVER_HOST = 'https://92.62.138.29';

const PUSH_SERVER_USER = null;

function saveUserToken($token)
{
    $url = PUSH_SERVER_HOST . "/api/messaging/bind-token";
    $context = stream_context_create([
        'http' => [
            'method' => 'POST',
            'header' => [
                'Content-Type: application/json'
            ],
            'content' => json_encode([
                'token' => PUSH_SERVER_USER,
                'instanceId' => $token
            ])
        ],
        'ssl' => [
            'verify_peer' => false,
            'verify_peer_name' => false
        ]
    ]);

    $response = file_get_contents($url,false, $context);

    $data = json_decode($response, true);

    return (bool) $data['success'];
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $content = file_get_contents('php://input');
    $data = json_decode($content, true);

    $res = saveUserToken($data['token']);

    if ($res) {
        die(json_encode(['status' => 'success']));
    }

    die(json_encode(['status' => 'error']));
}

