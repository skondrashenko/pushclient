<?php
    require_once "backend.php";
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <script src="https://www.gstatic.com/firebasejs/6.6.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/6.6.1/firebase-messaging.js"></script>
    <script>
        // Your web app's Firebase configuration
        var firebaseConfig = {
            apiKey: "AIzaSyAV1Rl7BVzG_tUKNaW5UZ2bkF-iQEMI24M",
            authDomain: "web-api-979ae.firebaseapp.com",
            databaseURL: "https://web-api-979ae.firebaseio.com",
            projectId: "web-api-979ae",
            storageBucket: "web-api-979ae.appspot.com",
            messagingSenderId: "512674192052",
            appId: "1:512674192052:web:89e4cf7d0c19d8c59955e1"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
        const messaging = firebase.messaging();
        messaging.usePublicVapidKey('BE33p16UnNeVP5laMgH3HdOTImyWBOP11NrjcHfCU2w1FKrpNOShMtBIBkuz3p_ULgzo8kp8IDrz5tT3qQjPBgE');

        messaging.onMessage((payload) => {
            console.log('Message received. ', payload);
        });

        Notification.requestPermission().then((permission) => {
            if (permission === 'granted') {
                messaging.getToken().then((token) => {
                    if (token) {
                        fetch('/', {
                            method: "POST",
                            body: JSON.stringify({
                                token
                            }),
                            credentials: "same-origin"
                        }).then(response => response.json())
                            .then(data => {
                                const {status} = data;
                                console.log(status === 'success' ? 'applied' : 'rejected')
                            })
                            .catch(() => console.error('Server Error'));
                    } else {
                        console.error('Token not Fetched!');
                    }
                });
            } else {
                console.error('Permissions not granted!');
            }
        });



    </script>
</body>
</html>